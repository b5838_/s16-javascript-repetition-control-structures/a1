// 3. Create a variable number that will store the value of the number provided by the user via the prompt.
let number = Number(prompt("Provide a Number: "));
console.log("The number you provide is " + number)

// 4. Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.
for(let x = number; x >= 0; x--){
    // 5. Create a condition that if the current value is less than or equal to 50, stop the loop.
    if(x <= 50){
        break;
    } 
    // 6. Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.
    if(x % 10 == 0){
        console.log("The number is divisible by 10. Skipping the number.");
        continue;
    }
    // 7. Create another condition that if the current value is divisible by 5, print the number.
    if(x % 5 == 0){
        console.log(x);
    }
}

// 8. Create a variable that will contain the string supercalifragilisticexpialidocious.
let word = "supercalifragilisticexpialidocious"
console.log(word);

// 9. Create another variable that will store the consonants from the string.
let consonant = "";

// 10. Create another for Loop that will iterate through the individual letters of the string based on it’s length.
for(i = 0; i<word.length; i++){
    // 11. Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.
    if(word[i].toLowerCase() == 'a' || word[i].toLowerCase() == 'e' || word[i].toLowerCase() == 'i' || word[i].toLowerCase() == 'o' || word[i].toLowerCase() == 'u'){
        continue;
    }
    // 12. Create an else statement that will add the letter to the second variable.
    else{
        consonant += word[i];
    }
} 

console.log(consonant);